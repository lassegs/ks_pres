
<!-- .slide: data-background="https://www.etpsmr.org/wp-content/uploads/2021/06/Horizon-Europe.png" data-background-size="33%" data-background-position="bottom" -->

# Samfunnssikkerhet

Muligheter for offentlig sektor i Horisont Europa CL3

---

![Pillars](https://www.zenit.de/wp-content/uploads/News/Struktur-Horizon-Europe.jpg)

---

## Norske søkere i HEU

<i class="ph-bold ph-currency-eur"></i> 488 mill. <small>(prosjektverdi 3.5mrd €)</small>

<i class="ph-bold ph-chart-pie-slice"></i> 3.35 % returandel <small>(Mål: 2.8 %)</small>

<i class="ph-bold ph-flag"></i> 23.6 % norsk suksessrate <small>(Europa: 16.2 %)</small>

<i class="ph-bold ph-medal"></i> 10 % av innstilte søknader

---

### Destinasjoner
HEUCL3: Civil Security for Society


- [Better protect the EU and its Citizens against Crime and Terrorism](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/better-protect-eu-and-its-citizens-against-crime-and-terrorism_en)
- [Effective Management of External Borders](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/effective-management-eu-external-borders_en)
- [Resilient Infrastructures](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/resilient-infrastructures_en)
- [Increased Cybersecurity](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/increased-cybersecurity_en)
- [A Disaster Resilient Society for Europe](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/disaster-resilient-society-europe_en)
- [Strengthened Security Research and Innovation](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/strengthened-security-research-and-innovation-ssri_en)


Note:

- Crime&Terror: For å sikre at kriminalitet og terrorisme blir mer effektivt bekjempet - samtidig so grunnleggende rettigheter respekteres - takket være mer kraftfull forebygging, beredskap og respons.
- Borders: For å sikre at lovlige passasjerer og forsendelser enklere kan reise inn i EU, samtidig som ulovlig handel, menneskesmugling, piratkopiering, terrorisme og andre kriminelle handlinger forebygges, takket være forbedret grensekontroll på luft, land og sjø, samt økt maritim sikkerhet.
- Infrastructures: For å styrke motstandskraften og autonomien til fysiske og digitale infrastrukturer, og for å sikre at viktige samfunnsfunksjoner opprettholdes, takket være kraftigere forebygging, beredskap og respons.
- Cybersecurity: For økt cybersikkerhet og en tryggere nettbasert miljø ved å utvikle og benytte EU og medlemsstatenes kapasiteter.
- Disaster: Dette betyr at tap fra naturkatastrofer, ulykker og menneskeskapte katastrofer reduseres gjennom forbedret forebygging av katastroferisiko basert på preventive tiltak, bedre samfunnsberedskap og motstandskraft, samt forbedret katastroferisikohåndtering på en systematisk måte.
- SSRI: Å generere kunnskap og verdi innen tverrgående saker for å unngå sektorspesifikk skjevhet og bryte ned barrierer som hindrer spredningen av felles sikkerhetsløsninger. Det vil også støtte innovasjon og strategier for markedspenetrasjon.

---

## <i class="ph-bold ph-clock"></i> 

<div class="halfsplit">
<div class="gridleft"> 

### 2023

 28. jun- 23. nov

- 186 mill. euro
- 34 utlysninger

</div>
<div class="gridright">

### 2024

 27. juni - 20. nov

- 196 mill. euro
- 25 utlysninger 

</div>
</div>



---


## Hvor finner man utlysningene?

[Funding & Tenders Portal](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-search;callCode=null;freeTextSearchKeyword=;matchWholeText=true;typeCodes=0,1,2,8;statusCodes=31094501;programmePeriod=null;programCcm2Id=43108390;programDivisionCode=43118971;focusAreaCode=null;destinationGroup=null;missionGroup=null;geographicalZonesCode=null;programmeDivisionProspect=null;startDateLte=null;startDateGte=null;crossCuttingPriorityCode=null;cpvCode=null;performanceOfDelivery=null;sortQuery=sortStatus;orderBy=asc;onlyTenders=false;topicListKey=topicSearchTablePageState)

[Arbeidsprogrammet 23-24](https://research-and-innovation.ec.europa.eu/document/download/ed4ea470-af89-49d7-85c1-f9bb3039ccbd_en)

---

<!-- .slide: data-background="https://www.etpsmr.org/wp-content/uploads/2021/06/Horizon-Europe.png" data-background-size="40%" data-background-position="right 75%" -->



### Muligheter for offentlig sektor

- Kompetanseheving
- Teknologiutvikling
- Standardisering
- Lage og tilby opplæring

---

## Hvorfor offentlig sektor

- 3 % av returandel til offentlig sektor <small>(alle klynger)</small>
- Attraktiv samarbeidspartner
- I CL3 ofte påkrevd

![firstresponder](https://i.imgur.com/4Y5QLjs.png)


---

## Offentlig sektor

<div class="halfsplit">
<div class="gridleft"> 

#### Roller i prosjektet

1. Leder, pådriver, problemutformer
2. Arbeidspakkeleder
3. Delta inn i arbeidspakke

</div>
<div class="gridright">

#### Innganger

1. <small>Egne problemstillinger opp mot tema i utlysning</small>
2. <small>Henvendelser fra andre miljøer som trenger sluttbruker</small>

</div>
</div>

---

<!-- .slide: data-background="https://pbs.twimg.com/media/Dr4xhs7WsAAz0hb.jpg" data-background-opacity="0.25"-->

## Politidirektoratet: iMARS

- 2020-2024 og 7 mill. euro
- Nye verktøy for effektiv verifisering av dokumentintegritet
- Algoritmer og dokumentasjonsverktøy​
    - Grense​, politi​, dokumentutsendelse​
- Opplæring​
- Database for trening og testing​
- Standardutvikling (ISO)

Note:
Basert på lysark fra Frøy Løvåsdal, seniorrådgiver, Politidirektoratet ​

---

<!-- .slide: data-background-color="black" -->

### Merverdi for Politiet

- Unik innsikt og kompetanse
- Bedre rustet til å håndtere dagens, og fremtidens, utfordringer
- Blant de første i verden...
- Deltakelse i eksperimenter
- Kompetanseheving
- Kalibrering av kompetanse

---

<!-- .slide: data-background-color="black" -->

## <i class="ph ph-chat-circle-text"></i>

- "Next level"
- "Gøy", "interessant", "utfordrendre"
- "Grusomt", "veldig vanskelig", "selvtillitsdreper", "aner ikke hva jeg driver med"

---

![Barriers](https://netzerocities.eu/wp-content/uploads/2022/04/Cities-Needs-Drivers-and-Barriers-1024x627.png)

---

## Horisont Europa kurstilbud

- [EU-rådgiversamling 27. september (påmelding/program)](https://forskningsradet.pameldingssystem.no/horisont-europa-samling-for-nasjonalt-stotteapparat-27-september-2023#/form)

- [Åpen kafe](https://www.forskningsradet.no/arrangementer/)


---

<!-- .slide: data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_INTROPOSTER_LONG_NAMETAG_2022-05-05-074407_pwrq.mp4" data-background-opacity="1"-->

---

<img src="https://i.imgur.com/ruVjmQ0.png" width="30%">

Lasse Gullvåg Sætre

[Muliggjørende Teknologier](https://www.forskningsradet.no/portefoljer/muliggjorende-teknologier/)

lgs@forskningsradet.no

[Forskningsrådet](forskningsradet.no)

